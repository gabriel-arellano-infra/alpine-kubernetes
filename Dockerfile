FROM alpine:3.18.3 as downloader

ARG KUBECTL_VERSION=1.28.2
ARG KUSTOMIZE_VERSION=5.0.3
# https://github.com/instrumenta/kubeval/
ARG KUBEVAL_VERSION=0.16.1
# https://github.com/yannh/kubeconform
ARG KUBECONFORM_VERSION=0.6.3

WORKDIR /tools

RUN apk add --no-cache \
    bash=5.2.15-r5 \
    curl=8.2.1-r0
SHELL ["/bin/bash", "-o", "pipefail", "-c"]
RUN curl -LsO https://dl.k8s.io/release/v${KUBECTL_VERSION}/bin/linux/amd64/kubectl \
    && chmod u+x kubectl \
    && curl -Ls https://github.com/instrumenta/kubeval/releases/download/v${KUBEVAL_VERSION}/kubeval-linux-amd64.tar.gz | tar xz \
    && curl -Ls https://github.com/yannh/kubeconform/releases/download/v${KUBECONFORM_VERSION}/kubeconform-linux-amd64.tar.gz | tar xz \
    && curl -LsO https://raw.githubusercontent.com/kubernetes-sigs/kustomize/master/hack/install_kustomize.sh \
    && chmod u+x ./install_kustomize.sh \
    && ./install_kustomize.sh ${KUSTOMIZE_VERSION}

FROM alpine:3.18.3

LABEL maintainer="gabrielarellano@gmail.com"

# Install utilities
RUN apk add --no-cache \
    jq=1.6-r3 \
    curl=8.2.1-r0 \
    bash=5.2.15-r5 \
    git=2.40.1-r0 \
    gettext=0.21.1-r7

COPY --from=downloader /tools/kubectl /usr/local/bin/kubectl
COPY --from=downloader /tools/kustomize /usr/local/bin/kustomize
COPY --from=downloader /tools/kubeval /usr/local/bin/kubeval
COPY --from=downloader /tools/kubeconform /usr/local/bin/kubeconform
