# Alpine kubernetes

Alpine docker image with utilities (bash, curl, jq, git) and kubernetes tools (kubectl, kubeval, kubeconform, kustomize).

## Use

```
docker pull registry.gitlab.com/gabriel-arellano-infra/alpine-kubernetes:{tag}
```

## Tags

| Tag    | Alpine | kubectl | kubeval | kustomize | kubeconform |
|--------|--------|---------|---------|-----------|-------------|
| latest | 3.18.3 | 1.28.2  | 0.16.1  | 5.0.3     | 0.6.3       |
| 1.28.2 | 3.18.3 | 1.28.2  | 0.16.1  | 5.0.3     | 0.6.3       |
| 1.27.1 | 3.17.3 | 1.27.1  | 0.16.1  | 5.0.1     | 0.6.1       |
| 1.23.5 | 3.15.3 | 1.23.5  | 0.16.1  | 4.5.4     |             |
| 1.23.4 | 3.15.1 | 1.23.4  | 0.16.1  | 4.5.2     |             | 

